<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

//  Pour avoir la configuration et les informations de connexion dans $infoBdd
require_once '../config/appConfig.php';
//  Pour utiliser les fonctions
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);
dump_var($db, DUMP, 'Objet PDO:');

if (!is_null($db)) {
    $res = getClubId($db, 1);
    dump_var($res, DUMP, 'getClubId(1):');

    $res = getClubId($db, 9999);
    dump_var($res, DUMP, 'getClubId(9999):');

    $res = getClubId($db, '9999');
    dump_var($res, DUMP, 'getClubId("9999"):');
} else {
    echo '<h1>Erreur de création de la connexion $db</h1>';
}