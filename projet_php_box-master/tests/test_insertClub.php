<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

//  Pour avoir la configuration et les informations de connexion dans $infoBdd
require_once '../config/appConfig.php';
//  Pour utiliser les fonctions
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infobdd);
dump_var($db,DUMP,'objet PDO:');

if (!is_null($bd)) {
    $dataClub = [
        'nomClub' => 'Club_' . rand(1, 99),
        'adresseClub' => rand(1, 20) . 'rue des clubs de boxe',
        'cpClub' => rand(69000, 69100),
        'villeClub' => 'Ville fictive'
    ];
    dump_var($dataClub, DUMP, '$dataClub:');
    $res = insertClub($db, $dataClub);
    dump_var($res, DUMP, 'resultat: ');

}else '<h1> Erreur de création de la connexion $db </h1>';