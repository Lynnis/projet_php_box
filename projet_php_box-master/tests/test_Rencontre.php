<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

require_once '../config/appConfig.php';

use Entities\Rencontre;

echo '<h1>Instanciation par défaut</h1>';
$obj = new Rencontre();
dump_var($obj, DUMP, 'Club par défaut:');

$tab = array (
    'idRencontre'=>1,
    'heureDebutRencontre'=>"12h",
    'heureFinRencontre'=>"14h",
    'idCompet'=>1,

);
echo '<h1>Instanciation avec toutes les infos </h1>';
$obj = new Rencontre($tab);
dump_var($obj, DUMP, 'Club avec toutes les valeurs:');

echo '<h1>Modification du numéro </h1>';
$obj->setIdRencontre(2);
dump_var($obj, DUMP, 'Club modifier:');
