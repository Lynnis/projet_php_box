<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

//  Pour avoir la configuration et les informations de connexion dans $infoBdd
//  Basculez la constante DUMP de appConfig à true pour les tests.
require_once '../config/appConfig.php';
//  Pour utiliser les fonctions
require_once '../src/fonctionsUtiles.php';

$db = connectBdd($infoBdd);
dump_var($db, DUMP, 'Objet PDO:');

if (!is_null($db)) {
    $nb = rand(1,5);
    dump_var($nb, DUMP, 'club qui va être modifié :');
    $res = getClubId($db, $nb);
    dump_var($res, DUMP, 'données du club avant la modification :');

    //Création des données modifié du club
    $datasClub = [
            'idClub' => $nb,
            'nomClub' => 'Club_'. rand(1,99),
            'adresseClub' => rand(1,20).'rue des clubs de boxe',
            'cpClub' => rand(69000,69100),
            'villeClub' => 'Ville fictive'
        ];


    $res = updateClub($db, $datasClub);
    dump_var($res, DUMP, 'données du club après modification:');

    $datasClub = [
        'idClub' => 100,
        'nomClub' => 'Club_'. rand(1,99),
        'adresseClub' => rand(1,20).'rue des clubs de boxe',
        'cpClub' => rand(69000,69100),
        'villeClub' => 'Ville fictive'
    ];


    $res = updateClub($db, $datasClub);
    dump_var($res, DUMP, 'données du club après modification ou insertion :');


} else {
    echo '<h1>Erreur de création de la connexion $db</h1>';
}