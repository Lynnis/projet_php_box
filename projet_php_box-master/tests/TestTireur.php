<?php

//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Pour forcer les dumps pendant les tests
define('DUMP', true);

require_once '../config/appConfig.php';

use Entities\Tireur;

echo '<h1>Instanciation par défaut</h1>';
$obj = new Tireur();
dump_var($obj, DUMP, 'Club par défaut:');

$tab = array (
    'idtireur'=>1,
    'nomtireurprenomtireur'=>"XIE XU",
    'datenaisstireur'=>"130 rue de l'ort",
    'sexetireur' => '69008',
    'poidstireur' => 'Lyon',
    'numlicencetireur' => 'Lyon');


echo '<h1>Instanciation avec toutes les infos </h1>';
$obj = new Tireur($tab);
dump_var($obj, DUMP, 'Club avec toutes les valeurs:');

echo '<h1>Modification du numéro </h1>';
$obj->setIdTireur(2);
dump_var($obj, DUMP, 'Club modifier:');

