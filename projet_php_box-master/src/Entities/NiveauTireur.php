<?php

namespace Entities;

class NiveauTireur
{
    private $idNivTireur = null;

    /**
     * @return null
     */
    public function getIdNivTireur() :?int
    {
        return $this->idNivTireur;
    }

    /**
     * @param null $idNivTireur
     */
    public function setIdNivTireur($idNivTireur): void
    {
        if($this->idNivTireur==NULL) {
            $this->idNivTireur = $idNivTireur;
        }
    }

    /**
     * @return mixed
     */
    public function getLibNivTireur() : string
    {
        return $this->LibNivTireur;
    }

    /**
     * @param mixed $LibNivTireur
     */
    public function setLibNivTireur($LibNivTireur): void
    {
        $this->LibNivTireur = $LibNivTireur;
    }
    private $LibNivTireur;

    public function __construct(array $datas = NULL)
    {
        if (!is_null($datas)) {
            (isset($datas['idNivTireur'])) ? $this->setIdNivTireur($datas['idNivTireur']) : $this->setIdNivTireur(null);
            (isset($datas['libNivTireur'])) ? $this->setLibNivTireur($datas['libNivTireur']) : $this->setLibNivTireur('');

        }

    }}


