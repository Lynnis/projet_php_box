<?php

namespace Entities;

class Rencontre {
    private $idRencontre = null;
    private $heureDebutRencontre;
    private $heureFinRencontre;
    private $idCompet;

    /**
     * @return mixed
     */
    public function getIdCompet()
    {
        return $this->idCompet;
    }

    /**
     * @param mixed $idCompet
     */
    public function setIdCompet($idCompet): void
    {
        $this->idCompet = $idCompet;
    }




    /**
     * @return null
     */
    public function getIdRencontre()
    {
        return $this->idRencontre;
    }

    /**
     * @param null $idRencontre
     */
    public function setIdRencontre($idRencontre): void
    {
        $this->idRencontre = $idRencontre;
    }

    /**
     * @return mixed
     */
    public function getHeureDebutRencontre()
    {
        return $this->heureDebutRencontre;
    }

    /**
     * @param mixed $heureDebutRencontre
     */
    public function setHeureDebutRencontre($heureDebutRencontre): void
    {
        $this->heureDebutRencontre = $heureDebutRencontre;
    }

    /**
     * @return mixed
     */
    public function getHeureFinRencontre()
    {
        return $this->heureFinRencontre;
    }

    /**
     * @param mixed $heureFinRencontre
     */
    public function setHeureFinRencontre($heureFinRencontre): void
    {
        $this->heureFinRencontre = $heureFinRencontre;
    }




    public function __construct(array $datas = NULL)
    {
        if (!is_null($datas)) {
            (isset($datas['idRencontre'])) ? $this->setIdRencontre($datas['idRencontre']) : $this->setIdRencontre(null);
            (isset($datas['heureDebutRencontre'])) ? $this->setHeureDebutRencontre($datas['heureDebutRencontre']) : $this->setHeureDebutRencontre('');
            (isset($datas['heureFinRencontre'])) ? $this->setHeureFinRencontre($datas['heureFinRencontre']) : $this->setHeureFinRencontre('');
            (isset($datas['idCompet'])) ? $this->setIdCompet($datas['idCompet']) : $this->setIdCompet(null);


        }

    }
}