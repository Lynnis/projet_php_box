<?php

namespace Entities;

class Tireur
{
    private $idTireur = null;
    private $nomTireur;

    /**
     * @return null
     */
    public function getIdTireur() :?int
    {
        return $this->idTireur;
    }

    /**
     * @param null $idTireur
     */
    public function setIdTireur($idTireur): void
    {
        if($this->idTireur==NULL) {
            $this->idTireur = $idTireur;
        }
    }

    /**
     * @return mixed
     */
    public function getNomTireur()
    {
        return $this->nomTireur;
    }

    /**
     * @param mixed $nomTireur
     */
    public function setNomTireur($nomTireur): void
    {
        $this->nomTireur = $nomTireur;
    }

    /**
     * @return mixed
     */
    public function getPrenomTireur()
    {
        return $this->prenomTireur;
    }

    /**
     * @param mixed $prenomTireur
     */
    public function setPrenomTireur($prenomTireur): void
    {
        $this->prenomTireur = $prenomTireur;
    }

    /**
     * @return mixed
     */
    public function getDateNaissTireur()
    {
        return $this->DateNaissTireur;
    }

    /**
     * @param mixed $DateNaissTireur
     */
    public function setDateNaissTireur($DateNaissTireur): void
    {
        $this->DateNaissTireur = $DateNaissTireur;
    }

    /**
     * @return mixed
     */
    public function getNumLicenceTireur()
    {
        return $this->NumLicenceTireur;
    }

    /**
     * @param mixed $NumLicenceTireur
     */
    public function setNumLicenceTireur($NumLicenceTireur): void
    {
        $this->NumLicenceTireur = $NumLicenceTireur;
    }

    /**
     * @return mixed
     */
    public function getSexeTireur()
    {
        return $this->SexeTireur;
    }

    /**
     * @param mixed $SexeTireur
     */
    public function setSexeTireur($SexeTireur): void
    {
        $this->SexeTireur = $SexeTireur;
    }

    /**
     * @return mixed
     */
    public function getPoidsTireur()
    {
        return $this->PoidsTireur;
    }

    /**
     * @param mixed $PoidsTireur
     */
    public function setPoidsTireur($PoidsTireur): void
    {
        $this->PoidsTireur = $PoidsTireur;
    }
    private $prenomTireur;
    private $DateNaissTireur;
    private $NumLicenceTireur;
    private $SexeTireur;
    private $PoidsTireur;
    public function __construct(array $datas = NULL)
    {
        if (!is_null($datas)) {
            (isset($datas['idtireur'])) ? $this->setIdTireur($datas['idtireur']) : $this->setIdTireur(null);
            (isset($datas['nomtireur'])) ? $this->setNomTireur($datas['nomtireur']) : $this->setNomTireur('');
            (isset($datas['prenomtireur'])) ? $this->setPrenomTireur($datas['prenomtireur']) : $this->setPrenomTireur('');
            (isset($datas['datenaisstireur'])) ? $this->setDateNaissTireur($datas['datenaisstireur']) : $this->setDateNaissTireur('');
            (isset($datas['sexetireur'])) ? $this->setSexeTireur($datas['sexetireur']) : $this->setSexeTireur('');
            (isset($datas['poidstireur'])) ? $this->setPoidsTireur($datas['poidstireur']) : $this->setPoidsTireur('');
            (isset($datas['numlicencetireur'])) ? $this->setNumLicenceTireur($datas['numlicencetireur']) : $this->setNumLicenceTireur('');

        }
    }
}

