<?php

namespace Entities;

class Club
{
    private $idClub = null;
    private $nomClub;
    private $adresseClub;
    private $cpClub;
    private $villeClub;

    /**
     * @return mixed
     */
    public function getIdClub() : ?int
    {
        return $this->idClub;
    }

    /**
     * @param mixed $idClub
     */
    public function setIdClub($idClub): void
    {
            if($this->idClub==NULL) {
                $this->idClub = $idClub;
            }
    }

    /**
     * @return mixed
     */
    public function getNomClub() : string
    {
        return $this->nomClub;
    }

    /**
     * @param mixed $nomClub
     */
    public function setNomClub($nomClub): void
    {
        $this->nomClub = $nomClub;
    }

    /**
     * @return mixed
     */
    public function getAdresseClub() : string
    {
        return $this->adresseClub;
    }

    /**
     * @param mixed $adresseClub
     */
    public function setAdresseClub($adresseClub): void
    {
        $this->adresseClub = $adresseClub;
    }

    /**
     * @return mixed
     */
    public function getCpClub() : string
    {
        return $this->cpClub;
    }

    /**
     * @param mixed $cpClub
     */
    public function setCpClub($cpClub): void
    {
        $this->cpClub = $cpClub;
    }

    /**
     * @return mixed
     */
    public function getVilleClub() : string
    {
        return $this->villeClub;
    }

    /**
     * @param mixed $villeClub
     */
    public function setVilleClub($villeClub): void
    {
        $this->villeClub = $villeClub;
    }

    public function __construct(array $datas = NULL)
    {
        if (!is_null($datas)) {
            (isset($datas['idClub'])) ? $this->setIdClub($datas['idClub']) : $this->setIdClub(null);
            (isset($datas['nomClub'])) ? $this->setNomClub($datas['nomClub']) : $this->setNomClub('');
            (isset($datas['adresseClub'])) ? $this->setAdresseClub($datas['adresseClub']) : $this->setAdresseClub('');
            (isset($datas['cpClub'])) ? $this->setCpClub($datas['cpClub']) : $this->setCpClub('');
            (isset($datas['villeClub'])) ? $this->setVilleClub($datas['villeClub']) : $this->setVilleClub('');
        }

    }

}