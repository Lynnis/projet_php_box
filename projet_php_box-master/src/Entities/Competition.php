<?php

namespace Entities;

class Competition
{
    private $idCompet = null;
    private $dateDebutCompet;

    /**
     * @return null
     */
    public function getIdCompet() : int
    {
        return $this->idCompet;
    }

    /**
     * @param null $idCompet
     */
    public function setIdCompet($idCompet): void
    {
        if($this->idCompet==NULL) {
        $this->idCompet = $idCompet;
    }
    }

    /**
     * @return mixed
     */
    public function getDateDebutCompet()
    {
        return $this->dateDebutCompet;
    }

    /**
     * @param mixed $dateDebutCompet
     */
    public function setDateDebutCompet($dateDebutCompet): void
    {
        $this->dateDebutCompet = $dateDebutCompet;
    }

    /**
     * @return mixed
     */
    public function getDateFinCompet()
    {
        return $this->dateFinCompet;
    }

    /**
     * @param mixed $dateFinCompet
     */
    public function setDateFinCompet($dateFinCompet): void
    {
        $this->dateFinCompet = $dateFinCompet;
    }
    private $dateFinCompet;

    public function __construct(array $datas = NULL)
    {
        if (!is_null($datas)) {
            (isset($datas['idCompet'])) ? $this->setIdCompet($datas['idCompet']) : $this->setIdCompet(null);
            (isset($datas['dateDebutCompet'])) ? $this->setDateDebutCompet($datas['dateDebutCompet']) : $this->setDateDebutCompet('');
            (isset($datas['dateFinCompet'])) ? $this->setDateFinCompet($datas['dateFinCompet']) : $this->setDateFinCompet('');
        }

    }
}