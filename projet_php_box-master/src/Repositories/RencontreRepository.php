<?php

namespace Repositories;


use Entities\Rencontre;

class RencontreRepository
{
    //attribut qui contient la connexion à la BDD
    protected $bdd;

    //constructeur permet d'aboir la chaine de connexion PDO
    public function __construct(\PDO $bdd){
        if(!is_null($bdd))
            $this->bdd = $bdd;
    }

    /* fonction qui donne la liste des clubs
     * @return array|null
     */

    public function getAll() : ? array
    {
        $resultSet = NULL;
        $query = 'SELECT * FROM rencontre';
        dump_var($query, DUMP, 'Requête SQL:');


        $rqtResult = $this->bdd->query($query);


        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                var_dump($row);
                //A chaque occurence de la BDD on crée un objet acteur qu'on insère dans le tableau resultSet
                $resultSet[] = new Rencontre($row);

            }
        }

        return $resultSet;
    }

    /* fonction qui recherche un club
     * @param int $id
     * @return Club|null
     */

    public function getById( int $id): ?Rencontre {
        $resultSet = NULL;
        $query = 'SELECT * FROM rencontre WHERE idRencontre =:idRecontre;';

        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idClub' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                // Si on récupère une occurence, on crée un objet acteur avec cette dernière
                $resultSet = new Club($tab);
            }
        }
        return $resultSet;
    }

    /* Fonction d'insertion d'un club
     * @param Club $entity
     * @return Club|null
     */

    public function insert(Rencontre $entity): ?Rencontre {
        $resultSet = NULL;


        $query = "INSERT INTO Rencontre" .
            " ( HeureDebutRencontre, HeureFinRencontre, lesRencontres)"
            . " VALUES (:heureDebutRencontre, :heureFinRencontre, :lesRencontres )";
        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [
                ':heureDebutRencontre' => $entity->getHeureDebutRencontre(),
                ':heureFinRencontre' => $entity->getHeureFinRencontre(),
                ':lesRencontres' => $entity->getLesRencontre(),
            ]

                . " WHERE idRencontre = :id");

            // on prepare la requête
            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Club');

            if ($res !== FALSE) {
                //Si la requête c'est bien éxécuté on récupère l'id généré en BDD et on met à jour l'id dans $entity
                $entity->setIdRencontre($this->bdd->lastInsertId());
                $resultSet = $entity;
            }

            return $resultSet;
        }

        /*Fonction de mise à jour d'un club
         * @param Club $entity
         * @return Club|null
         */
    public function update(Rencontre $entity): ?Rencontre {
        $resultSet = NULL;
        // On exécute l'update que si $entity a bien un numAct et que ce dernier existe en BDD
        if (is_null($entity->getIdRencontre()) || is_null($this->getById($entity->getIdRencontre()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE Rencontre"
                . " SET heureDebutRencontre=:heureDebutRencontre,"
                . "heureFinRencontre=:heureDebutRencontre"
                ."lesRencontres=:lesRencontres";
            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Rencontre');
            $res = $reqPrep->execute(
                [
                    ':heureDebutRencontre' => $entity->getHeureDebutRencontre(),
                    ':heureFinRencontre' => $entity->getHeureFinRencontre(),
                    ':idRencontre' => $entity ->getIdRencontre(),
                    ':lesRencontres' => $entity->getLesRencontre(),
                ]
            );

            if ($res !== FALSE) {
                // si tout c'est bien passé on met l'entité qui viens d'être mis à jour dans resultSet
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    /**
     * Fait un insert ou un update du site selon la clé du Club
     * @param
     * @return array|null
     */
    function save(Rencontre $entity): ?Rencontre {
        dump_var($entity, DUMP, '$entity dans save');
        if ($entity->getIdNivTireur())
            return $this->update($entity);
        else
            return $this->insert($entity);
    }

}
