<?php

namespace Repositories;

use Entities\Tireur;

class TireurRepository{

    //attribut qui contient la connexion à la BDD
    protected $bdd;

    //constructeur permet d'aboir la chaine de connexion PDO
    public function __construct(\PDO $bdd){
        if(!is_null($bdd))
            $this->bdd = $bdd;
    }

    /* fonction qui donne la liste des clubs
     * @return array|null
     */

    public function getAll() : ? array
    {
        $resultSet = NULL;
        $query = 'SELECT * FROM tireur';
        dump_var($query, DUMP, 'Requête SQL:');


        $rqtResult = $this->bdd->query($query);


        if ($rqtResult) {
            $rqtResult->setFetchMode(\PDO::FETCH_ASSOC);
            foreach($rqtResult as $row ) {
                //A chaque occurence de la BDD on crée un objet acteur qu'on insère dans le tableau resultSet
                $resultSet[] = new Tireur($row);

            }
        }

        return $resultSet;
    }

    /* fonction qui recherche un club
     * @param int $id
     * @return Club|null
     */

    public function getById( int $id): ?Tireur {
        $resultSet = NULL;
        $query = 'SELECT * FROM tireur WHERE id =:idNivTireur;';

        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute([':idNivTireur' => $id]);

        if ($res !== FALSE) {
            $tab = ($tmp = $reqPrep->fetch(\PDO::FETCH_ASSOC)) ? $tmp : null;
            if(!is_null($tab)) {
                // Si on récupère une occurence, on crée un objet acteur avec cette dernière
                $resultSet = new NiveauTireur($tab);
            }
        }
        return $resultSet;
    }

    /* Fonction d'insertion d'un club
     * @param Club $entity
     * @return Club|null
     */

    public function insert(NiveauTireur $entity): ?NiveauTireur {
        $resultSet = NULL;


        $query = "INSERT INTO niveautireur" .
            " ( libniveauTireur)"
            . " VALUES (:libnivtireur )";
        // On prépare la rêquete
        $reqPrep = $this->bdd->prepare($query);

        $res = $reqPrep->execute(
            [
                ':libnivtireur' => $entity->getLibNivTireur(),

            ]
        );

        if ($res !== FALSE) {
            //Si la requête c'est bien éxécuté on récupère l'id généré en BDD et on met à jour l'id dans $entity
            $entity->setIdClub($this->bdd->lastInsertId());
            $resultSet = $entity;
        }

        return $resultSet;
    }

    /*Fonction de mise à jour d'un club
     * @param Club $entity
     * @return Club|null
     */
    public function update(NiveauTireur $entity): ?NiveauTireur {
        $resultSet = NULL;
        // On exécute l'update que si $entity a bien un numAct et que ce dernier existe en BDD
        if (is_null($entity->getIdNivTireur()) || is_null($this->getById($entity->getIdNivTireur()))) {
            $resultSet = NULL;
        } else {
            //  Entité existante
            $query = "UPDATE niveautireyr"
                . " SET libNivTireur=:libNivTireur, "

                . " WHERE idNivTireur = :id";

            // on prepare la requête
            $reqPrep = $this->bdd->prepare($query);
            dump_var($reqPrep, DUMP, '$reqPrep dans update Club');
            $res = $reqPrep->execute(
                [
                    ':libNivTireur' => $entity->getLibNivTireur(),

                ]
            );

            if ($res !== FALSE) {
                // si tout c'est bien passé on met l'entité qui viens d'être mis à jour dans resultSet
                $resultSet = $entity;
            }
        }

        return $resultSet;
    }

    /**
     * Fait un insert ou un update du site selon la clé du Club
     * @param Club $entity
     * @return array|null
     */
    function save(NiveauTireur $entity): ?Club {
        dump_var($entity, DUMP, '$entity dans save');
        if ($entity-> getIdNivTireur())
            return $this->update($entity);
        else
            return $this->insert($entity);
    }


}