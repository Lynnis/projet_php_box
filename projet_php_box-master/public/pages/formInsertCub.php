<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';


?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Partie 2 </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
include_once '../inc/menu.php';
?>

<section id="corps">
    <h1> Ajouter un Club </h1> <!--A modifier pour la partie Optimisation !-->
    </header>

    <form method="POST" action="../traits/traitInsertClub.php">
        <input type="hidden" id="idClub" name="idClub" value="<?= $club['idClub'] ?>" /> <!--Permet de garder l'id quand on envoie les données du formulaire !-->
        <div>
            <label for="nomClub">Nom du club :</label><br/>
            <input type="text" id="nomClub" placeholder="nom du club" name="nomClub" size="40">
        </div>
        <div>
            <label for="adresseClub">Adresse du club :</label><br/>
            <input type="text"id="adresseClub" placeholder="Adresse du club" name="adresseClub" size="100" required="required">
        </div>
        <div>
            <label for="cpClub">Code postal du club :</label><br/>
            <input type="text"id="cpClub" placeholder="Code postal du club" name="cpClub" size="20" required="required">
        </div>
        <div>
            <label for="villeClub">Ville du club :</label><br/>
            <input type="text" id="villeClub" placeholder="Ville du club" name="villeClub" size="40" required="required">
        </div>
        <br/>
        <div class="form-group">
            <button type="submit"> 'Ajouter'</button>
        </div>

    </form>

</section>

<div class="footer-container">
    <?php include_once '../inc/footer.php'; ?>
</div>

<script src="js/kickstart.js"></script> <!-- KICKSTART -->
<script src="js/main.js"></script>
</body>
</html>

