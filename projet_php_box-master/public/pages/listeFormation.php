<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../config/appConfig.php';
require_once '../src/fonctionsUtiles.php';

 
    $db = connectBdd($infoBdd);
    $repo = new Repositories\FormationRepository($db);
    $listeFormation = $repo->getAll();
   
?>
<!DOCTYPE html>
<HEAD>
    <TITLE> FORMATION </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="css/style.css">

</HEAD>
	<?php
    include_once 'inc/header.php';
    include_once 'inc/menu.php';
    ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">Vous utilisez un navigateur web <strong>dépassé</strong>. S'il vous plait, <a href="http://browsehappy.com/">mettez à jour votre navigateur</a> pour améliorer votre experience de navigation.</p>
        <![endif]-->

        <div class="header-container">
	    <?php include_once 'inc/header.php'; ?>
	</div>

        <main class="main-container">
            <div class="main wrapper clearfix">
		<div id="corps"><!--  C'est cette div qui doit être personalisé sur chaque page  -->
		    <article>
			<header>
			    <h1>Liste des sites</h1>
			</header>
			<section>
			    <?php if (!is_null($listeFormation)): ?>
    			    <table class="striped sortable">
    				<thead>
    				    <tr><th>Code</th><th>libellé</th><th>durée</th><th>num Formateur</th></tr>
    				</thead>
    				<tbody>
				    <?php
				    foreach ($listeFormation as $form):
					?>
					<tr>
					    <td><?= $form->getCodeFormation(); ?></td>
					    <td><?= $form->getLibelleFormation(); ?></td>
					    <td><?= $form->getDureeFormation();?></td>
					    <td><?= $form->getNumFormateur();?></td>
					</tr>
				    <?php endforeach; ?>
    				</tbody>
    			    </table>
			    <?php else: ?>
    			    <p>Oups... Il semble y avoir eu une erreur!</p>
			    <?php endif; ?>
			</section>
		    </article>
		</div><!-- id="corps" -->
            </div>
        </main> <!-- #main-container -->

        <div class="footer-container">
	    <?php include_once 'inc/footer.php'; ?>
        </div>

	<script src="js/kickstart.js"></script> <!-- KICKSTART -->
        <script src="js/main.js"></script>
    </body>
</html>