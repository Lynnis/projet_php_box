<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';
use Entities\Club;
use Repositories\ClubRepository;

//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
$bdd = connectBdd($infoBdd);

//Si la connexion réussi alors j'appelle ma fonction getAllClubs sinon $lesClubs est null
if ( $bdd) {
    $repo = new Repositories\ClubRepository($bdd);



    $lesClubs = $repo->getAll();
} else {
    $lesClubs = null;
}
?>
    <!DOCTYPE html>
    <HTML>
    <HEAD>
        <TITLE> 1SIO - TP PHP Exercice 2 - Recupérer les clubs </TITLE>
        <meta charset="UTF-8">
        <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

    </HEAD>
    <BODY>
    <?php
    include_once '../inc/header.php';
    include_once '../inc/menu.php';
    ?>
    <section id="corps">
        <h1>LISTES DES CLUBS</h1>
        <p>Un petit extrait de notre base de données </p>
        <?php if (!is_null($lesClubs)): ?> <!-- Permet de faire la suite du code uniquement si j'ai récupèrer des données !-->
            <table id='table2'>
                <thead>
                <tr><th>Id</th><th>Nom du club</th><th>Adresse du club</th><th>Code postal</th><th>Ville</th></tr>
                </thead>
                <tbody>
                <!-- Vous devez  parcourir votre tableau lesClubs et pour chaque enregistrement l'afficher  dans votre tableau HTML !-->
                <?php
                foreach ($lesClubs as $club)
                { ?>
                    <tr>
                        <td><?php echo $club->getIdClub();?></td>
                        <td><?php echo $club->getNomCLub();?></td>
                        <td><?php echo $club->getAdresseClub();?></td>
                        <td><?php echo $club->getCpClub();?></td>

                        <td><?php echo $club->getVilleClub();?></td>


                    </tr>
                    <?php //fin de votre boucle
                }?>
                </tbody>
            </table>
        <?php else: ?>
            <p>Oups... Il semble y avoir eu une erreur!</p>
        <?php endif; ?>
    </section>
    <?php
    include_once '../inc/footer.php';
    ?>
    </body>
    </html>

<?php
