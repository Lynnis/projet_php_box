<?php
declare(strict_types=1);

require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';


?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <TITLE> 1SIO - TP PHP Partie 2 </TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="screen"type="text/css" href="../css/style.css">

</HEAD>
<BODY>
<?php
include_once '../inc/header.php';
include_once '../inc/menu.php';
?>

<section id="corps">
    <h1> Rencontre </h1> <!--A modifier pour la partie Optimisation !-->
    </header>

    <form method="POST" action="../traits/traitInsertClub.php">
        <div>
            <label for="nomClub">Heure début de la rencontre :</label><br/>
            <input type="text" id="heuredebutrencontre" placeholder="heure debut rencontre" name="heure debut rencontre" size="40">
        </div>
        <br>
        <div>
            <label for="nomClub">Heure fin de la rencontre :</label><br/>
            <input type="text" id="heurefinrencontre" placeholder="heure fin rencontre" name="heure fin rencontre" size="40">
        </div>
        <br/>
        <div class="form-group">
            <button type="submit">Ajouter</button>
        </div>

    </form>

</section>

<div class="footer-container">
    <?php include_once '../inc/footer.php'; ?>
</div>

<script src="js/kickstart.js"></script> <!-- KICKSTART -->
<script src="js/main.js"></script>
</body>
</html>


