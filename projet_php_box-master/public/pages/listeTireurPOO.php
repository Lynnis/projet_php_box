<?php
//  Permet d'utiliser le typage fort. !! Laisser en première ligne !!
declare(strict_types=1);

//  Les inclusions nécessaires
require_once '../../config/appConfig.php';
require_once '../../src/fonctionsUtiles.php';
use Entities\Club;
use Repositories\ClubRepository;

//  Par principe, mettez le maximum du code PHP nécessaire ici.
//Connexion à la BDD
$bdd = connectBdd($infoBdd);

//Si la connexion réussi alors j'appelle ma fonction getAllClubs sinon $lesClubs est null
if ( $bdd) {
    $repo = new Repositories\TireurRepository($bdd);



    $lestireur = $repo->getAll();
} else {
    $lestireur = null;
}
?>

  <div class="header-container">
	</div>

        <main class="main-container">
            <div class="main wrapper clearfix">
		<div id="corps"><!--  C'est cette div qui doit être personalisé sur chaque page  -->
		    <article>
			<header>
			    <h1>Liste des sites</h1>
			</header>
			<section>
			    <?php if (!is_null($lestireur)): ?>
    			    <table class="striped sortable">
    				<thead>
    				    <tr><th>Code</th><th>libellé</th><th>durée</th><th>num Formateur</th></tr>
    				</thead>
    				<tbody>
				    <?php
				    foreach ($lestireur as $form):
					?>
					<tr>
					    <td><?= $form->getIdTireur(); ?></td>
					    <td><?= $form->getnomTireur(); ?></td>
					    <td><?= $form->getdateNaissTireur();?></td>
					    <td><?= $form->getnumLicenceTireur();?></td>
                        <td><?= $form->getsexeTireur();?></td>
                        <td><?= $form->getpoidsTireur();?></td>
					</tr>
				    <?php endforeach; ?>
    				</tbody>
    			    </table>
			    <?php else: ?>
    			    <p>Oups... Il semble y avoir eu une erreur!</p>
			    <?php endif; ?>
			</section>
		    </article>
		</div><!-- id="corps" -->
            </div>
        </main> <!-- #main-container -->

        <div class="footer-container">

        </div>

	<script src="js/kickstart.js"></script> <!-- KICKSTART -->
        <script src="js/main.js"></script>
    </body>
</html>